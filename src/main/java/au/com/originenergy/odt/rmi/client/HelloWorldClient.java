package au.com.originenergy.odt.rmi.client;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import au.com.odt.api.rmi.HelloService;

/**
 * The Class HelloWorldClient.
 * 
 * Originally sourced from <a href='https://github.com/muff1nman/java-rmi-example.git'>https://github.com/muff1nman/java-rmi-example.git</a>
 */
public class HelloWorldClient { 
    public static void main(String args[]) throws Exception {
        Registry registry = LocateRegistry.getRegistry("localhost");
        HelloService obj = (HelloService) registry.lookup("HelloService");
        System.out.println(obj.getMessage()); 
    }
}