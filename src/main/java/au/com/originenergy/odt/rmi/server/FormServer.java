package au.com.originenergy.odt.rmi.server;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.odata4j.consumer.ODataConsumer;
import org.odata4j.core.Guid;
import org.odata4j.core.OEntity;
import org.odata4j.core.OProperties;
import org.odata4j.core.OProperty;

import au.com.odt.api.rmi.FormService;
import au.com.odt.api.sales.Form;
import au.com.odt.api.sales.FormAttributeValue;

/**
 * The Class FormServer.
 * 
 * Originally sourced from <a href='https://github.com/muff1nman/java-rmi-example.git'>https://github.com/muff1nman/java-rmi-example.git</a>
 */
public class FormServer implements FormService {

	/** The Constant serviceUrl. */
	static final String serviceUrl = "http://%s:%s/sap/opu/odata/sap/CUSTOMER_CONTACT/";

	/**
	 * Save form with form attribute values.
	 *
	 * @param form the form
	 * @return the form
	 * @throws RemoteException the remote exception
	 */
	@Override
	public Form saveFormWithFormAttributeValues(Form form) throws RemoteException {
		try {
			ODataConsumer c = OriginRMIServer.buildODataConsumer(serviceUrl);

			List<OEntity> attributes = new ArrayList<OEntity>();
			
			for (String typeId : Arrays.asList(new String[] { 
					"ENQUIRY_TY", 
					"ENQUIRY_Q", 
					"FIRST_NAME", 
					"LAST_NAME",
					"CONTACT_TI", 
					"EMAIL", 
					"ACCOUNT", 
					"PHONE",
					"BUSI_NAME", 
					"STREET", 
					"SUBURB", 
					"STATE", 
					"POSTCODE"
			})) {
				for (FormAttributeValue formAttributeValue : form.getFormAttributeValueList()) {
					if (typeId.equals(formAttributeValue.getAttributeTypeId())){
						OEntity fa = c 
						        .createEntity("FormAttributeValues")
						        .properties(OProperties.string("AttributeID", formAttributeValue.getAttributeTypeId()))
						        .properties(OProperties.string("Value", formAttributeValue.getValue()))
						        .get();
						
						attributes.add(fa);
					}
				}
			}
			
			OEntity oform = c
					.createEntity("Forms")
					.properties(OProperties.string("TypeID", form.getTypeId()))
					.inline("Attributes", attributes)
					.execute();
			
			OProperty<Guid> formId = (OProperty<Guid>) oform.getProperty("FormID");
			form.setFormId(formId.getValue().toString());
		}
		catch(Throwable t) {
			System.out.print(t.getMessage());
		}

		return form;
	}

}