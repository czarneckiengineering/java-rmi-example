package au.com.originenergy.odt.rmi.server;

import java.rmi.RemoteException;

import au.com.odt.api.rmi.HelloService;

/**
 * The Class HelloServer.
 * 
 * Originally sourced from <a href='https://github.com/muff1nman/java-rmi-example.git'>https://github.com/muff1nman/java-rmi-example.git</a>
 */
public class HelloServer implements HelloService {

	/** The Constant MESSAGE. */
	private static final String MESSAGE = "Hello world";

	/**
	 * Gets the message.
	 *
	 * @return the message
	 * @throws RemoteException the remote exception
	 */
	public String getMessage() throws RemoteException {
		return MESSAGE;
	}

	
}