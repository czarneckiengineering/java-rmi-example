package au.com.originenergy.odt.rmi.server;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

import org.odata4j.consumer.ODataConsumer;
import org.odata4j.consumer.ODataConsumers;
import org.odata4j.consumer.behaviors.OClientBehavior;

import au.com.odt.api.rmi.FormService;
import au.com.odt.api.rmi.HelloService;
import au.com.odt.api.rmi.PropertiesService;

/**
 * The Class OriginRMIServer.
 * 
 * Originally sourced from <a href='https://github.com/muff1nman/java-rmi-example.git'>https://github.com/muff1nman/java-rmi-example.git</a>
 */
public class OriginRMIServer {

	/**
	 * 	SALES.service=http://%s:%s/sap/opu/odata/sap/SALES/
	 * 	CONSUMPTION_MODEL.service=http://%s:%s/sap/opu/odata/sap/CONSUMPTION_MODEL/
	 * 	PRODUCT_CATALOG.service=http://%s:%s/sap/opu/odata/sap/PRODUCT_CATALOG/
	 * 	PAYMENT.service=http://%s:%s/sap/opu/odata/sap/PAYMENT/
	 * 	BILLING_PAYMENT.service=http://%s:%s/sap/opu/odata/sap/BILLING_PAYMENT/
	 * 	CUSTOMER_SERVICE.service=http://%s:%s/sap/opu/odata/sap/CUSTOMER_SERVICE/
	 * 	CUSTOMER_CONTACT.service=http://%s:%s/sap/opu/odata/sap/CUSTOMER_CONTACT/
	 *
	 * @throws RemoteException the remote exception
	 */

	public OriginRMIServer() throws RemoteException {

	}
	
	/**
	 * The main method.
	 *
	 * @param args the arguments
	 * @throws Exception the exception
	 */
	public static void main(String args[]) throws Exception {

		// if(System.getSecurityManager()==null){
		// System.setSecurityManager(new RMISecurityManager());
		// }
		System.out.println("RMI server started");

		// Instantiate RmiServer
		try { // special exception handler for registry creation

			Registry reg;
			
			try {
				reg = LocateRegistry.createRegistry(1099);
				System.out.println("java RMI registry created.");

			} catch (Exception e) {
				System.out.println("Using existing registry");
				reg = LocateRegistry.getRegistry();
			}
			
			HelloServer helloServer = new HelloServer();
			HelloService helloService = (HelloService) UnicastRemoteObject.exportObject(helloServer, 0);
			reg.rebind("HelloService", helloService);
			
			PropertiesServer propertiesServer = new PropertiesServer();
			PropertiesService propertiesService = (PropertiesService) UnicastRemoteObject.exportObject(propertiesServer, 0);
			reg.rebind("PropertiesService", propertiesService);
			
			FormServer formServer = new FormServer();
			FormService formService = (FormService) UnicastRemoteObject.exportObject(formServer, 0);
			reg.rebind("FormService", formService);

		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Builds the o data consumer.
	 *
	 * @param serviceUrl the service url
	 * @return the o data consumer
	 */
	public static ODataConsumer buildODataConsumer(String serviceUrl) {
		String server = PropertiesServer.getProperty("digital.server");
        String port = PropertiesServer.getProperty("digital.port");
		serviceUrl = String.format(serviceUrl, server, port);

        String userid = PropertiesServer.getProperty("digital.userid");
		String password = PropertiesServer.getProperty("digital.password");
		OClientBehavior xCsrfAuth = new XCsrfTokenBehavior(userid, password);
    	
        ODataConsumer c = ODataConsumers.newBuilder(serviceUrl).setClientBehaviors(xCsrfAuth).build();
		
		return c;
	}

}
