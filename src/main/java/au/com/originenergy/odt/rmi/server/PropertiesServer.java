package au.com.originenergy.odt.rmi.server;

import java.rmi.RemoteException;
import java.util.Properties;

import au.com.odt.api.rmi.PropertiesService;

/**
 * The Class PropertiesServer.
 * 
 * Originally sourced from <a href='https://github.com/muff1nman/java-rmi-example.git'>https://github.com/muff1nman/java-rmi-example.git</a>
 */
public class PropertiesServer implements PropertiesService {
	
	/** The properties. */
	private static Properties properties;

	/**
	 * Sets the properties.
	 *
	 * @param properties the new properties
	 * @throws RemoteException the remote exception
	 */
	@Override
	public void setProperties(Properties properties) throws RemoteException {
		PropertiesServer.properties = properties;
	}
	
	/**
	 * Gets the property.
	 *
	 * @param key the key
	 * @return the property
	 */
	public static String getProperty (String key) {
		String returnValue = (String) properties.get(key);
		
		return returnValue;
	}

}
