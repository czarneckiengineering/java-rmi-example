/**
 * 
 */
package au.com.originenergy.odt.rmi.server;

import java.rmi.RemoteException;
import java.util.List;

import au.com.odt.api.payment.AuthorizationCode;
import au.com.odt.api.productcatalogue.EnergyProductTariff;
import au.com.odt.api.productcatalogue.OfferedDivision;
import au.com.odt.api.productcatalogue.PredictedEnergyCost;
import au.com.odt.api.productcatalogue.Product;
import au.com.odt.api.rmi.SignupService;
import au.com.odt.api.sales.ContactPerson;
import au.com.odt.api.sales.CustomerInfo;
import au.com.odt.api.sales.EmailContactInfo;
import au.com.odt.api.sales.OrderAddress;
import au.com.odt.api.sales.OrderDate;
import au.com.odt.api.sales.OrderHeader;
import au.com.odt.api.sales.OrderItem;
import au.com.odt.api.sales.OrderItemStatus;
import au.com.odt.api.sales.PhoneInfo;
import au.com.odt.api.sales.ServiceProviderAppointment;
import au.com.odt.api.sales.ValidatedAddressInfo;
import au.com.odt.api.shared.PaymentCardTokenInfo;

/**
 * @author czarnem
 *
 */
public class SignupServer implements SignupService {

	/* (non-Javadoc)
	 * @see au.com.odt.api.rmi.SignupService#createAdditionalProductLeads(au.com.odt.api.sales.OrderHeader, java.lang.String)
	 */
	@Override
	public OrderHeader createAdditionalProductLeads(OrderHeader arg0,
			String arg1) throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see au.com.odt.api.rmi.SignupService#createEmailAddressinCustomerInfo(au.com.odt.api.sales.EmailContactInfo)
	 */
	@Override
	public void createEmailAddressinCustomerInfo(EmailContactInfo arg0)
			throws RemoteException {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see au.com.odt.api.rmi.SignupService#createOrderForNewCustomer()
	 */
	@Override
	public OrderHeader createOrderForNewCustomer() throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see au.com.odt.api.rmi.SignupService#createOrderForNewCustomerByCustomerType(java.lang.String)
	 */
	@Override
	public OrderHeader createOrderForNewCustomerByCustomerType(String arg0)
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see au.com.odt.api.rmi.SignupService#createOrderHeaderWithAddress(au.com.odt.api.sales.OrderHeader)
	 */
	@Override
	public OrderHeader createOrderHeaderWithAddress(OrderHeader arg0)
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see au.com.odt.api.rmi.SignupService#createOrderItem(java.lang.String, au.com.odt.api.sales.OrderItem)
	 */
	@Override
	public OrderItem createOrderItem(String arg0, OrderItem arg1)
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see au.com.odt.api.rmi.SignupService#createPhoneNumbersinCustomerInfo(au.com.odt.api.sales.PhoneInfo)
	 */
	@Override
	public void createPhoneNumbersinCustomerInfo(PhoneInfo arg0)
			throws RemoteException {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see au.com.odt.api.rmi.SignupService#deleteAdditionalProductLeads(au.com.odt.api.sales.OrderHeader, java.lang.String)
	 */
	@Override
	public OrderHeader deleteAdditionalProductLeads(OrderHeader arg0,
			String arg1) throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see au.com.odt.api.rmi.SignupService#deleteOrderAddress(java.lang.String, java.lang.String)
	 */
	@Override
	public void deleteOrderAddress(String arg0, String arg1)
			throws RemoteException {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see au.com.odt.api.rmi.SignupService#deleteOrderItem(java.lang.String, java.lang.String)
	 */
	@Override
	public void deleteOrderItem(String arg0, String arg1)
			throws RemoteException {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see au.com.odt.api.rmi.SignupService#getAvailableAppointments(java.lang.String, java.lang.String)
	 */
	@Override
	public List<ServiceProviderAppointment> getAvailableAppointments(
			String arg0, String arg1) throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see au.com.odt.api.rmi.SignupService#getAvailableServiceConnectionDates(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public List<OrderDate> getAvailableServiceConnectionDates(String arg0,
			String arg1, String arg2) throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see au.com.odt.api.rmi.SignupService#getCardTokenisationAuthorizationCode(java.lang.String, java.lang.String)
	 */
	@Override
	public AuthorizationCode getCardTokenisationAuthorizationCode(String arg0,
			String arg1) throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see au.com.odt.api.rmi.SignupService#getDualFuelProductByDivision(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public Product getDualFuelProductByDivision(String arg0, String arg1,
			String arg2, String arg3, String arg4, String arg5)
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see au.com.odt.api.rmi.SignupService#getDualFuelProductByDivisionId(java.lang.String, java.lang.String)
	 */
	@Override
	public Product getDualFuelProductByDivisionId(String arg0, String arg1)
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see au.com.odt.api.rmi.SignupService#getEligibleProductsByAddress(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public List<Product> getEligibleProductsByAddress(String arg0, String arg1,
			String arg2, String arg3, String arg4, String arg5, String arg6,
			String arg7, String arg8, String arg9) throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see au.com.odt.api.rmi.SignupService#getOfferedDivisionByAddress(java.lang.String, java.lang.String)
	 */
	@Override
	public List<OfferedDivision> getOfferedDivisionByAddress(String arg0,
			String arg1) throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see au.com.odt.api.rmi.SignupService#getOrderAddressByPremiseID(java.lang.String)
	 */
	@Override
	public ValidatedAddressInfo getOrderAddressByPremiseID(String arg0)
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see au.com.odt.api.rmi.SignupService#getOrderWithAllChildren(java.lang.String, java.lang.String)
	 */
	@Override
	public OrderHeader getOrderWithAllChildren(String arg0, String arg1)
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see au.com.odt.api.rmi.SignupService#getOrderWithAllChildrenByReferenceNumber(java.lang.String)
	 */
	@Override
	public List<OrderItemStatus> getOrderWithAllChildrenByReferenceNumber(
			String arg0) throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see au.com.odt.api.rmi.SignupService#getPaymentPreAuthorizationCode(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public AuthorizationCode getPaymentPreAuthorizationCode(String arg0,
			String arg1, String arg2) throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see au.com.odt.api.rmi.SignupService#getPredictedEnergyCostForOrderItem(java.lang.String, java.lang.String)
	 */
	@Override
	public List<PredictedEnergyCost> getPredictedEnergyCostForOrderItem(
			String arg0, String arg1) throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see au.com.odt.api.rmi.SignupService#getProductIdByCampaign(java.lang.String, java.lang.String)
	 */
	@Override
	public Product getProductIdByCampaign(String arg0, String arg1)
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see au.com.odt.api.rmi.SignupService#getProductTariffForOrder(java.lang.String, java.lang.String)
	 */
	@Override
	public List<EnergyProductTariff> getProductTariffForOrder(String arg0,
			String arg1) throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see au.com.odt.api.rmi.SignupService#getProductTariffForOrderByReferenceNumber(java.lang.String, java.lang.String)
	 */
	@Override
	public List<EnergyProductTariff> getProductTariffForOrderByReferenceNumber(
			String arg0, String arg1) throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see au.com.odt.api.rmi.SignupService#getProductWithProductAttributes(java.lang.String)
	 */
	@Override
	public Product getProductWithProductAttributes(String arg0)
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see au.com.odt.api.rmi.SignupService#getSPAppointmentRequired(java.lang.String)
	 */
	@Override
	public boolean getSPAppointmentRequired(String arg0) throws RemoteException {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see au.com.odt.api.rmi.SignupService#saveBusinessOrderContactDetails(au.com.odt.api.sales.OrderHeader)
	 */
	@Override
	public OrderHeader saveBusinessOrderContactDetails(OrderHeader arg0)
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see au.com.odt.api.rmi.SignupService#saveContactPerson(au.com.odt.api.sales.ContactPerson)
	 */
	@Override
	public ContactPerson saveContactPerson(ContactPerson arg0)
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see au.com.odt.api.rmi.SignupService#saveCustomerBankAccountDetails(au.com.odt.api.sales.OrderHeader)
	 */
	@Override
	public boolean saveCustomerBankAccountDetails(OrderHeader arg0)
			throws RemoteException {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see au.com.odt.api.rmi.SignupService#saveCustomerBillingPreferences(au.com.odt.api.sales.OrderHeader)
	 */
	@Override
	public boolean saveCustomerBillingPreferences(OrderHeader arg0)
			throws RemoteException {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see au.com.odt.api.rmi.SignupService#saveCustomerConcessionCardDetails(au.com.odt.api.sales.OrderHeader)
	 */
	@Override
	public boolean saveCustomerConcessionCardDetails(OrderHeader arg0)
			throws RemoteException {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see au.com.odt.api.rmi.SignupService#saveOrderAddress(java.lang.String, au.com.odt.api.sales.OrderAddress)
	 */
	@Override
	public OrderAddress saveOrderAddress(String arg0, OrderAddress arg1)
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see au.com.odt.api.rmi.SignupService#saveOrderItem(java.lang.String, au.com.odt.api.sales.OrderItem)
	 */
	@Override
	public OrderItem saveOrderItem(String arg0, OrderItem arg1)
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see au.com.odt.api.rmi.SignupService#savePaymentCardInfo(au.com.odt.api.sales.OrderHeader, au.com.odt.api.shared.PaymentCardTokenInfo)
	 */
	@Override
	public boolean savePaymentCardInfo(OrderHeader arg0,
			PaymentCardTokenInfo arg1) throws RemoteException {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see au.com.odt.api.rmi.SignupService#saveResidentialOrderContactDetails(au.com.odt.api.sales.OrderHeader)
	 */
	@Override
	public OrderHeader saveResidentialOrderContactDetails(OrderHeader arg0)
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see au.com.odt.api.rmi.SignupService#updateCustomerInfo(au.com.odt.api.sales.CustomerInfo)
	 */
	@Override
	public CustomerInfo updateCustomerInfo(CustomerInfo arg0)
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see au.com.odt.api.rmi.SignupService#updateOrderHeader(au.com.odt.api.sales.OrderHeader)
	 */
	@Override
	public OrderHeader updateOrderHeader(OrderHeader arg0)
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

}
