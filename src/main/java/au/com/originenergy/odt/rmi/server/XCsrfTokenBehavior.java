package au.com.originenergy.odt.rmi.server;


import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.NewCookie;

import org.odata4j.consumer.ODataClientRequest;
import org.odata4j.jersey.consumer.behaviors.JerseyClientBehavior;
import org.odata4j.repack.org.apache.commons.codec.binary.Base64;

import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientRequest;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.filter.ClientFilter;
import com.sun.jersey.api.client.filter.Filterable;

public class XCsrfTokenBehavior implements JerseyClientBehavior {

	private String user;
	private String password;

	private String xCsrfToken;

	private Map<String, String> cookies = new HashMap<String, String>();

	public XCsrfTokenBehavior(String user, String password) {
		this.user = user;
		this.password = password;
	}

	@Override
	public ODataClientRequest transform(ODataClientRequest request) {
		String auth = user + ":" + password;

		byte[] bytes = auth.getBytes(Charset.forName("US-ASCII"));
		byte[] encodedAuth = Base64.encodeBase64(bytes);

		String authHeader = "Basic " + new String(encodedAuth);

		request = request.header("Authorization", authHeader);

		request = request.header("x-csrf-token", (xCsrfToken == null) ? "Fetch"	: xCsrfToken);
		
		if (cookies != null) {
			String cookieHeader = "";
			
			for(String key : cookies.keySet()) {
				cookieHeader += key + "=" + cookies.get(key) + ";";
			}
			
			if (!cookieHeader.equals("")) {
				request = request.header("Cookie", cookieHeader);
			}
		}

		return request;
	}

	@Override
	public void modifyClientFilters(Filterable client) {

		 client.addFilter(new ClientFilter() {

			@Override
			public ClientResponse handle(ClientRequest clientRequest)throws ClientHandlerException {
				ClientResponse response = getNext().handle(clientRequest);
				MultivaluedMap<String, String> headers = response.getHeaders();
				
				String newXCsrfToken = headers.getFirst("x-csrf-token");
				
				xCsrfToken = (newXCsrfToken != null) ? newXCsrfToken : xCsrfToken;

				List<NewCookie> responseCookies = response.getCookies();
				
				for (NewCookie cookie : responseCookies) {
					cookies.put(cookie.getName(), cookie.getValue());
				}
				
				return response;
			}
		 });
	}

	@Override
	public void modify(ClientConfig clientConfig) {}

	@Override
	public void modifyWebResourceFilters(Filterable webResource) {}

}